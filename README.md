Capture
=======

A tcpdump and daemonlogger packet capture front end for security analysts

Usage
====
    NAME
           capture - capture is a tool for network security analysts to simplify
           starting, listing, and stopping full packet captures.

    SYNOPSIS
           capture [-h?lsmv] [r] [-a analyst -d 'quoted description' -e 'quoted
           expression'] [-f filter]

    OPTIONS
           -a, --analyst
                   When creating a new full packet capture file, capture will use
                   the analyst name in the name of the capture file

           -c, --checksum
                   After stopping the capture, create an md5sum of the completed
                   capture file

           -d, --description
                   When creating a new full packet capture file, capture will
                   append the analyst provided description string to the file name

           -e, --expression
                   Follow the -e flag with quoted tcpdump syntax used to start the
                   capture.

           -f, --filter
                   When stopping or listing captures, a filter can be provided to
                   display only matching content.

           -h, --help
                   Print Options and Argumetns

           -l, --list
                   List running captures

           -m, --man
                   Print complete man page

           -r, --ringbuffer
                   Acquite completed capture by reading and filtering ongoing
                   daemonlogger traffic, and start a new capture with identical
                   criteria

           -s, --stop
                   Stop captures based on filter

           -v, --verbose
                   Make all output verbose

           -V, --Version
                   Print version and exit

           -z, --debug
                   Print all debug output

    DESCRIPTION
           Capture initiates, lists, and stops full packet captures using tcpdump
           while keeping analysts concerns on the incident at hand. It is only
           likley to work as expected in a GNU userland because of how it parses
           ps output.

           Capture will automatically store full packet captures in a default
           active capture path and build the analyst name, time-stamp, and tcpdump
           expression directly into the filename.

           Capture can also print active packet capture listings using filters.

           Stopping captures provides analysts and automated programs the ability
           to create md5 sums of completed captures and moves completed captures
           to specified locations.

    EXAMPLES
           Starting a capture:

           capture -a grant -d 'capturing all traffic to or from a host' -e 'host
           10.10.10.10'

           Listing captures:

           capture -l

           Verbosely listing captures started by an analyst named fred:

           capture -lf fred

           Stoping all of the caps found in the previous listing:

           capture -sf fred

           Listing captures described as torrent captures in the description:

           capture -lf torrent

           You should notice a trend - to stop the above listing of captures:

           capture -sf torrent

           To verbosely stop all captures described as irc captures and create
           .md5 checksums:

           capture -svcf irc

    ABOUT
           capture is by Grant Stavely: grant.stavely@constellation.com
